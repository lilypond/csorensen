@c -*- coding: utf-8; mode: texinfo; -*-
@c This file is part of lilypond.tely
@ignore
    Translation of GIT committish: 0d5071774c7990f75685c18c732f293b8336ae6c

    When revising a translation, copy the HEAD committish of the
    version that you are working on.  See TRANSLATION for details.
@end ignore

@c \version "2.11.38"

@c A menu is needed before every deeper *section nesting of @node's; run
@c     M-x texinfo-all-menus-update
@c to automatically fill in these menus before saving changes

@node Non-musical notation
@chapter Non-musical notation

Esta sección se ocupa de temas generales sobre lilypond, no de
notación específica.

@menu
* Titles and headers::          
* MIDI output::                 
* other midi::                  
@end menu


@node Titles and headers
@section Titles and headers

Casi toda la música impresa tiene un título y el nombre del
compositor; ciertas piezas tienen mucha más información.

@menu
* Creating titles::             
* Custom titles::               
* Reference to page numbers::   
* Table of contents::           
@end menu


@node Creating titles
@subsection Creating titles

Se crean títulos para cada uno de los bloques @code{\score} así como
para todo el archivo de entrada (o bloque @code{\book} (libro)).

El contenido de cada título se extrae a partir de los bloques
@code{\header} (cabecera).  El bloque de cabecera de un libro
contempla lo siguiente:

@table @code
@funindex dedicatoria
@item dedication
El dedicatario de la música, centrado en lo alto de la primera página.

@funindex title
@item title
El título de la música, centrado justo debajo de la
dedicatoria.

@funindex subtitle
@item subtitle
Subtítulo, centrado debajo del título.

@funindex subsubtitle
@item subsubtitle
Sub-subtítulo, centrado debajo del subtítulo.

@funindex poet
@item poet
Nombre del poeta, a la izquierda, debajo del subtítulo.

@funindex composer
@item composer
Nombre del compositor, a la derecha, debajo del subtítulo.

@funindex meter
@item meter
Texto de la medida, a la izquierda, debajo del poeta.

@funindex opus
@item opus
Nombre del Opus, a la derecha, debajo del compositor.

@funindex arranger
@item arranger
Nombre del arreglista, a la derecha, debajo del Opus.

@funindex instrument
@item instrument
Nombre del instrumento, centrado, debajo del arreglista.  También
aparece centrado en lo alto de cada página (aparte de la primera).

@funindex piece
@item piece
Nombre de la pieza, a la izquierda, debajo del instrumento.

@cindex página, saltos, forzar
@funindex breakbefore
@item breakbefore
Esto fuerza que el título comience sobre una página nueva (establézcalo a ##t o a ##f).

@funindex copyright
@item copyright
Aviso de Copyright, centrado en la parte inferior de la primera
página.  Para insertar el símbolo de copyright symbol, consulte
@ref{Text encoding}.

@funindex tagline
@item tagline
Cartel de propósito general en el pie de página, centrado al final de la última.

@end table

A continuación presentamos una demostración de todos los campos que
están disponibles.  Fíjese en que puede utilizar en la cabecera
cualquier instrucción de marcado de texto (Véase @ref{Formatting text}).

@lilypond[quote,verbatim,line-width=11.0\cm]
\paper {
  line-width = 9.0\cm
  paper-height = 10.0\cm
}

\book {
  \header {
    dedication = "dedicated to me"
    title = \markup \center-align { "Title first line" "Title second line,
longer" }
    subtitle = "the subtitle,"
    subsubtitle = #(string-append "subsubtitle LilyPond version "
(lilypond-version))
    poet = "Poet"
    composer =  \markup \center-align { "composer" \small "(1847-1973)" }
    texttranslator = "Text Translator"
    meter = \markup { \teeny "m" \tiny "e" \normalsize "t" \large "e" \huge
"r" }
    arranger = \markup { \fontsize #8.5 "a" \fontsize #2.5 "r" \fontsize
#-2.5 "r" \fontsize #-5.3 "a" \fontsize #7.5 "nger" }
    instrument = \markup \bold \italic "instrument"
    piece = "Piece"
  }

  \score {
    { c'1 }
    \header {
      piece = "piece1"
      opus = "opus1"
    }
  }
  \markup {
      and now...
  }
  \score {
    { c'1 }
    \header {
      piece = "piece2"
      opus = "opus2"
    }
  }
}
@end lilypond

Como se mostró anteriormente, puede usar varios bloques
@code{\header}.  Cuando los mismos campos aparecen en distintos
bloques, tiene validez el último en definirse.  A continuación un
breve ejemplo.

@example
\header @{
  composer = "Compositor"
@}
\header @{
  piece = "Pieza"
@}
\score @{
  \new Staff @{ c'4 @}
  \header @{
    piece = "Otra pieza"  % sobreescribe a la anterior
  @}
@}
@end example

Si define la cabecera @code{\header} dentro del bloque @code{\score},
por lo general se imprimirán solamente las cabeceras @code{piece} y
@code{opus}.  Fíjese en que la expresión musical debe aparecer antes
del @code{\header}.

@lilypond[quote,verbatim,line-width=11.0\cm]
\score {
  { c'4 }
  \header {
    title = "title"  % not printed
    piece = "piece"
    opus = "opus"
  }
}
@end lilypond

@funindex printallheaders
@noindent
Se puede cambiar este comportamiento (e imprimir todas las cabeceras
cuando la cabecera @code{\header} está definida dentro del bloque
@code{\score}) mediante la utilización de

@example
\paper@{
  printallheaders=##t
@}
@end example

@cindex copyright
@cindex tagline

El pie de página por omisión se encuentra vacío, excepto para la
primera página, en la que se inserta el campo de @code{copyright}
extraído del @code{\header}, y para la última página, en la que se
escribe el cartel @code{tagline} sacado del bloque @code{\header}.  La
línea por omisión para este cartel es @qq{Music engraving by LilyPond
(@var{version})}.@footnote{Las partituras bien tipografiadas son una
magnífica promoción para nosotros, así que por favor, si puede,
conserve intacta la línea del letrero.}

Las cabeceras se pueden quitar completamente estableciendo su valor a
falso.

@example
\header @{
  tagline = ##f
  composer = ##f
@}
@end example


@node Custom titles
@subsection Custom titles

Una opción más avanzada es cambiar las definiciones de las variables
siguientes dentro del bloque @code{\paper} block.  El archivo de
inicio @file{ly/titling-init.ly} ofrece un listado de la disposición
predeterminada.

@table @code
@funindex bookTitleMarkup
@item bookTitleMarkup
  Es el título que se escribe en lo alto del documento de salida
completo.  Normalmente contiene el compositor y el título de la pieza.

@funindex scoreTitleMarkup
@item scoreTitleMarkup
  Es el título que se coloca por encima de un bloque @code{\score}.
normalmente contiene el nombre del movimiento (campo @code{piece}).

@funindex oddHeaderMarkup
@item oddHeaderMarkup
  Es el encabezamiento de las páginas de numeración impar.

@funindex evenHeaderMarkup
@item evenHeaderMarkup
  Es el encabezamiento de las páginas de numeración par.  Si se deja
  sin especificar, se usará el encabezamiento de las páginas impares.

  De forma predeterminada, los encabezamientos se definen de tal forma
que el número de página está en el borde exterior, y el isntrumento
está centrado.

@funindex oddFooterMarkup
@item oddFooterMarkup
  Es el pie de las páginas de numeración impar.

@funindex evenFooterMarkup
@item evenFooterMarkup
  Es el pie de las páginas de numeración par.  Si se deja sin
  especificar, se usará en su lugar el encabezamiento de las páginas
  impares.

  De forma predeterminada, el pie de página tiene el aviso de
copyright en la primera página, y el cartel @code{tagline} en la
última.
@end table


@cindex \paper
@cindex encabezamiento
@cindex header
@cindex pie
@cindex disposición de la página
@cindex títulos

La definición siguiente pone el título en el extremo izquierdo, y el
compositor en el extremo derecho sobre una única línea.

@verbatim
\paper {
  bookTitleMarkup = \markup {
   \fill-line {
     \fromproperty #'header:title
     \fromproperty #'header:composer
   }
  }
}
@end verbatim

@node Reference to page numbers
@subsection Reference to page numbers

Se puede marcar un lugar determinado de una partitura utilizando la
instrucción @code{\label} (etiqueta), bien en lo alto de la estructura
o bien dentro de la música.  Posteriormente se puede hacer referencia
a esta etiqueta dentro de un elemento de marcado, para obtener el
número de la página en que se encuentra la marca, usando la
instrucción de marcado @code{\page-ref}.

@lilypond[verbatim,line-width=11.0\cm]
\header { tagline = ##f }
\book {
  \label #'firstScore
  \score {
    {
      c'1
      \pageBreak \mark A \label #'markA
      c'
    }
  }

  \markup { The first score begins on page \page-ref #'firstScore "0" "?" }
  \markup { Mark A is on page \page-ref #'markA "0" "?" }
}
@end lilypond

La instrucción de marcado @code{\page-ref} toma tres argumentos:
@enumerate
@item la etiqueta, un símbolo de scheme, p.ej. @code{#'firstScore};
@item un elemento de marcado que se usará como medidor para estimar las dimensiones del marcado;
@item un elemento de marcado que se utilizará en sustitución del número de página si la etiqueta es desconocida.
@end enumerate

El motivo de que se necesite un medidor es que en el momento en que se
están interpretando los marcados, los saltos de página aún no se han
producido y por tanto los números de página no se conocen todavía.
Para sortear este inconveniente, la interpretación real del marcado se
retrasa hasta un momento posterior; sin embargo, las dimensiones del
marcado se tienen que conocer de antemano, así que se usa el medidor
para decidir estas dimensiones.  Si el libro tiene entre 10 y 99
páginas, el medidor puede ser "00", es decir, un número de dos
dígitos.

@predefined

@funindex \label
@code{\label}
@funindex \page-ref
@code{\page-ref}

@node Table of contents
@subsection Table of contents
Se puede insertar un índice general o tabla de contenidos utilizando
la instrucción @code{\markuplines \table-of-contents}.  Los elementos
que deben aparecer en la tabla de contenidos se introducen con la
instrucción @code{\tocItem}, que se puede usar en el nivel más alto de
la jerarquía del código, o dentro de una expresión musical.

@verbatim
\markuplines \table-of-contents
\pageBreak

\tocItem \markup "Primera partitura"
\score { 
  {
    c'  % ...
    \tocItem \markup "Un punto concreto dentro de la primera partitura"
    d'  % ... 
  }
}

\tocItem \markup "Segunda partitura"
\score {
  {
    e' % ...
  }
}
@end verbatim

Los elementos de marcado que se usan para dar formato al índice
general se encuentran definidos dentro del bloque @code{\paper}.  Los
elementos predeterminados son @code{tocTitleMarkup}, para dar formato
al título de la tabla, y @code{tocItemMarkup}, para aplicar formato a
los elementos del índice, que consisten en el título del elemento y el
número de página.  Estas variables se pueden cambiar por parte del
usuario:

@verbatim
\paper {
  %% Traducir el título del índice al español:
  tocTitleMarkup = \markup \huge \column {
    \fill-line { \null "Índice general" \null }
    \hspace #1
  }
  %% usar una fuente mayor
  tocItemMarkup = \markup \large \fill-line {
    \fromproperty #'toc:text \fromproperty #'toc:page
  }
}
@end verbatim

Observe la forma en que nos referimos al texto y al número de página
del elemento de índice, dentro de la definición @code{tocItemMarkup}.

También se pueden definir nuevas instrucciones y elementos de
marcado para crear índices generales más eleborados:
@itemize
@item en primer lugar, defina una variable de marcado nueva dentro del bloque @code{\paper}
@item luego defina una función de música cuyo propósito es añadir un elemento al índice general
utilizando esta variable de marcado del bloque @code{\paper}.
@end itemize

En el ejemplo siguiente se define un estilo nuevo para introducir los
nombres de los actos de una ópera en el índice general:

@verbatim
\paper {
  tocActMarkup = \markup \large \column {
    \hspace #1
    \fill-line { \null \italic \fromproperty #'toc:text \null }
    \hspace #1
  }
}

tocAct = 
#(define-music-function (parser location text) (markup?)
   (add-toc-item! 'tocActMarkup text))
@end verbatim

@lilypond[line-width=11.0\cm]
\header { tagline = ##f }
\paper {
  tocActMarkup = \markup \large \column {
    \hspace #1
    \fill-line { \null \italic \fromproperty #'toc:text \null }
    \hspace #1
  }
}

tocAct = 
#(define-music-function (parser location text) (markup?)
   (add-toc-item! 'tocActMarkup text))

\book {
  \markuplines \table-of-contents
  \tocAct \markup { Atto Primo }
  \tocItem \markup { Coro. Viva il nostro Alcide }
  \tocItem \markup { Cesare. Presti omai l'Egizzia terra }
  \tocAct \markup { Atto Secondo }
  \tocItem \markup { Sinfonia }
  \tocItem \markup { Cleopatra. V'adoro, pupille, saette d'Amore }
  \markup \null
}
@end lilypond

@seealso

Archivos de inicio: @file{ly/@/toc@/-init@/.ly}.

@predefined

@funindex \table-of-contents
@code{\table-of-contents}
@funindex \tocItem
@code{\tocItem}

@node MIDI output
@section MIDI output

@cindex sonido
@cindex MIDI

El MIDI (Musical Instrument Digital Interface, Interfase Digital para
Instrumentos Musicales) es un estándar para interconectar y controlar
instrumentos musicales electrónicos.  Un archivo o secuencia MIDI es
una serie de notas dentro de un conjunto de pistas.  No es un archivoo
de sonidos reales; se necesita un programa reproductor especial para
traducir la serie de notas en sonidos de verdad.

Cualquier música se puede convertir a archivos MIDI, de manera que
podamos escuchar lo que hayamos introducido.  Esto es muy conveniente
para comprobar la corrección de la música; las octavas equivocadas o
las alteraciones erróneas se ponen de relieve muy claramente al
escuchar la salida MIDI.

@knownissues

Muchhos efectos con interés musical como el swing, la articulación, el
fraseo, etc., no se traducen al midi.

La salida midi reserva un canal para cada pentagrama, y uno más para
los ajustes globales.  Por ello, el archivo de entrada no debería
tener más de 15 pentagramas (o 14 si no usa percusión).  Los otros
pentagramas permanecerán en silencio.

No todos los reproductores de midi manejan correctamente los cambios
de tempo en la salida midi.  Entre los reproductores que se sabe que
funcionan, se encuentra
@uref{http://@/timidity@/.sourceforge@/.net/,timidity}.

@menu
* Creating MIDI files::         
* MIDI block::                  
* MIDI instrument names::       
* What goes into the MIDI?  FIXME::  
@end menu

@node Creating MIDI files
@subsection Creating MIDI files

Para crear un MIDI a partir de una pieza de música, escriba un bloque
@code{\midi} en la partitura, por ejemplo:

@example
\score @{
  @var{...música...}
   \midi @{
     \context @{
       \Score
       tempoWholesPerMinute = #(ly:make-moment 72 4)
       @}
     @}
@}
@end example

El tempo se puede especificar utilizando la instrucción @code{\tempo}
dentro de la propia música, véase @ref{Metronome marks}.  Más abajo se
muestra una alternativa que no da lugar a una indicación metronómica
en la partitura impresa.  En este ejemplo, el tempo de negras se
establece en 72 pulsos por minuto.  Esta clase de especificación del
tempo no puede tomar notas con puntillo como argumento.  En este caso,
divida las notas con puntillo en unidades más pequeñas.  Por ejemplo,
un tempo de 90 negras con puntillo por minuto se puede especificar
como 270 corcheas por minuto:

@example
tempoWholesPerMinute = #(ly:make-moment 270 8)
@end example

Si hay una instrucción @code{\midi} dentro de un @code{\score},
solamente se producirá MIDI.  Si se necesita también una notación
impresa, se debe escribir un bloque @code{\layout}

@example
\score @{
  @var{...música...}
  \midi @{ @}
  \layout @{ @}
@}
@end example
@cindex layout, bloque



Se interpretan las ligaduras de unión, los matices dinámicos y los
cambios de tempo.  Las marcas dinámicas, crescendi y decrescendi se
traducen en niveles de volumen MIDI.  Las marcas de dinámica se
traducen a una fracción fija del rango de volumen MIDI disponible, los
crescendi y decrescendi hacen que el volumen varíe de forma lineal
entre sus dos extremos.  Las fracciones se pueden ajustar mediante
@code{dynamicAbsoluteVolumeFunction} dentro de un contexto
@rinternals{Voice}.  Para cada tipo de instrumento MIDI se puede
definir un rango de volumen.  Esto proporciona un control de
ecualización básico, que puede realzar significativamente la calidad
de la salida MIDI.  El ecualizador se puede controlar estableciendo un
valor para @code{instrumentEqualizer}, o fijando

@example
\set Staff.midiMinimumVolume = #0.2
\set Staff.midiMaximumVolume = #0.8
@end example

Para quitar los matices dinámicos de la salida MIDI, escriba las
siguientes líneas dentro de la sección @code{\midi@{@}}.

@example
\midi @{
  ...
  \context @{
    \Voice
    \remove "Dynamic_performer"
  @}
@}
@end example


@knownissues

Los (de)crescendos sin terminar no se procesarán adecuadamente en el
archivo midi, dando como resultado pasajes de música en silenco.  Una
forma de sortearlo es terminar el (de)crescendo explícitamente.  Por
ejemmplo,

@example
@{ a\< b c d\f @}
@end example

@noindent
no funcionará bien, pero

@example
@{ a\< b c d\!\f @}
@end example

@noindent
sí lo hará.


Solamente se crea una salida MIDI cuando la instrucción @code{\midi}
está dentro de un bloque @code{\score}.  Si lo escribe dentro de un
contexto instanciado explícitamente (es decir: @code{\new Score} ) el
archivo no se producirá.  Para solucionarlo, encierre el @code{\new
Score} y el @code{\midi} dentro de un bloque @code{\score}.

@example
\score @{
  \new Score @{ @dots{}notas@dots{} @}
  \midi
@}
@end example


@node MIDI block
@subsection MIDI block
@cindex bloque MIDI


El bloque @code{\midi} es similar al bloque @code{\layout}, pero algo
más simple.  Puede contener definiciones de contexto.


@cindex contexto, definición de

Las definiciones de contexto siguen con exactitud la misma sintaxis
que dentro del bloque \layout.  Los módulos de traducción para el
sonido reciben el nombre de «performers» o intérpretes.  Los contextos
para la salida MIDI se encuentran definidos dentro de
@file{ly/@/performer@/-init@/.ly}.


@node MIDI instrument names
@subsection MIDI instrument names

@cindex instrumento, nombres de
@funindex Staff.midiInstrument

El nombre del instrumento MIDI se establece mediante la propiedad
@code{Staff.midiInstrument}.  El nombre del instrumento se debe elegir
de entre los que están en la lista que aparece en @ref{MIDI
instruments}.

@example
\set Staff.midiInstrument = "glockenspiel"
@var{...notes...}
@end example

Si el instrumento elegido no coincide exactamente con uno de los
instrumentos de la lista de instrumentos MIDI, se usará el instrumento
Piano de Cola (@code{"acoustic grand"}).


@node What goes into the MIDI?  FIXME
@subsection What goes into the MIDI?  FIXME

@menu
* Repeats and MIDI::            
@end menu

@node Repeats and MIDI
@subsubsection Repeats and MIDI

@cindex repeticiones, expandir
@funindex \unfoldRepeats

Con un poco de trucaje, se puede hacer que cualquier tipo de
repetición esté presente en la salida MIDI.  Esto se consigue mediante
la aplicación de la función musical @code{\unfoldRepeats}.  Esta
función cambia todas las repeticiones a repeticiones desplegadas.

@lilypond[quote,verbatim,fragment,line-width=8.0\cm]
\unfoldRepeats {
  \repeat tremolo 8 {c'32 e' }
  \repeat percent 2 { c''8 d'' }
  \repeat volta 2 {c'4 d' e' f'}
  \alternative {
    { g' a' a' g' }
    {f' e' d' c' }
  }
}
\bar "|."
@end lilypond

Al crear un archivo de partitura que use @code{\unfoldRepeats} para el
MIDI, es necesario hacer dos bloques @code{\score}: uno para el MIDI
(con repeticiones desplegadas) y otro para la notación impresa (con
repeticiones de volta --primera y segunda vez--, tremolo --trémolo--,
y percent --repetición de compases--).  Por ejemplo,

@example
\score @{
  @var{..música..}
  \layout @{ .. @}
@}
\score @{
  \unfoldRepeats @var{..música..}
  \midi @{ .. @}
@}
@end example


@node other midi
@section other midi

Los microtonos también se exportan hacia el archivo MIDI.




