@c -*- coding: utf-8; mode: texinfo; -*-
@c This file is part of lilypond.tely
@ignore
    Translation of GIT committish: FILL-IN-HEAD-COMMITTISH

    When revising a translation, copy the HEAD committish of the
    version that you are working on.  See TRANSLATION for details.
@end ignore

@c \version "2.11.38"

@node Notation manual tables
@appendix Notation manual tables

@menu
* Chord name chart::            
* MIDI instruments::            
* List of colors::              
* The Feta font::               
* Note head styles::            
* Text markup commands::        
* Text markup list commands::   
* List of articulations::       
* All context properties::      
* Layout properties::           
* Identifiers::                 
* Scheme functions::            
@end menu



@node Chord name chart
@appendixsec Chord name chart

The following charts shows two standard systems for printing chord
names, along with the pitches they represent.

@lilypondfile{chord-names-jazz.ly}

@node MIDI instruments
@appendixsec MIDI instruments

The following is a list of names that can be used for the
@code{midiInstrument} property.

@example
acoustic grand            contrabass           lead 7 (fifths)
bright acoustic           tremolo strings      lead 8 (bass+lead)
electric grand            pizzicato strings    pad 1 (new age)
honky-tonk                orchestral strings   pad 2 (warm)
electric piano 1          timpani              pad 3 (polysynth)
electric piano 2          string ensemble 1    pad 4 (choir)
harpsichord               string ensemble 2    pad 5 (bowed)
clav                      synthstrings 1       pad 6 (metallic)
celesta                   synthstrings 2       pad 7 (halo)
glockenspiel              choir aahs           pad 8 (sweep)
music box                 voice oohs           fx 1 (rain)
vibraphone                synth voice          fx 2 (soundtrack)
marimba                   orchestra hit        fx 3 (crystal)
xylophone                 trumpet              fx 4 (atmosphere)
tubular bells             trombone             fx 5 (brightness)
dulcimer                  tuba                 fx 6 (goblins)
drawbar organ             muted trumpet        fx 7 (echoes)
percussive organ          french horn          fx 8 (sci-fi)
rock organ                brass section        sitar
church organ              synthbrass 1         banjo
reed organ                synthbrass 2         shamisen
accordion                 soprano sax          koto
harmonica                 alto sax             kalimba
concertina                tenor sax            bagpipe
acoustic guitar (nylon)   baritone sax         fiddle
acoustic guitar (steel)   oboe                 shanai
electric guitar (jazz)    english horn         tinkle bell
electric guitar (clean)   bassoon              agogo
electric guitar (muted)   clarinet             steel drums
overdriven guitar         piccolo              woodblock
distorted guitar          flute                taiko drum
guitar harmonics          recorder             melodic tom
acoustic bass             pan flute            synth drum
electric bass (finger)    blown bottle         reverse cymbal
electric bass (pick)      shakuhachi           guitar fret noise
fretless bass             whistle              breath noise
slap bass 1               ocarina              seashore
slap bass 2               lead 1 (square)      bird tweet
synth bass 1              lead 2 (sawtooth)    telephone ring
synth bass 2              lead 3 (calliope)    helicopter
violin                    lead 4 (chiff)       applause
viola                     lead 5 (charang)     gunshot
cello                     lead 6 (voice)
@end example


@node List of colors
@appendixsec List of colors

@subsubheading Normal colors

Usage syntax is detailed in @ref{Coloring objects}.

@cindex List of colors
@cindex Colors, list of

@verbatim
black       white          red         green
blue        cyan           magenta     yellow
grey        darkred        darkgreen   darkblue
darkcyan    darkmagenta    darkyellow
@end verbatim


@subsubheading X color names

X color names come several variants:

Any name that is spelled as a single word with capitalization
(e.g. @q{LightSlateBlue}) can also be spelled as space separated
words without capitalization (e.g. @q{light slate blue}).

The word @q{grey} can always be spelled @q{gray} (e.g. @q{DarkSlateGray}).

Some names can take a numerical suffix (e.g. @q{LightSalmon4}).


@subsubheading Color Names without a numerical suffix:

@verbatim
snow		GhostWhite	WhiteSmoke	gainsboro	FloralWhite
OldLace		linen		AntiqueWhite	PapayaWhip	BlanchedAlmond
bisque		PeachPuff	NavajoWhite	moccasin	cornsilk
ivory		LemonChiffon	seashell	honeydew	MintCream
azure		AliceBlue	lavender	LavenderBlush	MistyRose
white		black		DarkSlateGrey	DimGrey		SlateGrey
LightSlateGrey	grey		LightGrey	MidnightBlue	navy
NavyBlue	CornflowerBlue	DarkSlateBlue	SlateBlue	MediumSlateBlue
LightSlateBlue	MediumBlue	RoyalBlue	blue		DodgerBlue
DeepSkyBlue	SkyBlue		LightSkyBlue	SteelBlue	LightSteelBlue
LightBlue	PowderBlue	PaleTurquoise	DarkTurquoise	MediumTurquoise
turquoise	cyan		LightCyan	CadetBlue	MediumAquamarine
aquamarine	DarkGreen	DarkOliveGreen	DarkSeaGreen	SeaGreen
MediumSeaGreen	LightSeaGreen	PaleGreen	SpringGreen	LawnGreen
green		chartreuse	MediumSpringGreen	GreenYellow	LimeGreen
YellowGreen	ForestGreen	OliveDrab	DarkKhaki	khaki
PaleGoldenrod	LightGoldenrodYellow	LightYellow	yellow	gold
LightGoldenrod	goldenrod	DarkGoldenrod	RosyBrown	IndianRed
SaddleBrown	sienna		peru		burlywood	beige
wheat		SandyBrown	tan		chocolate	firebrick
brown		DarkSalmon	salmon		LightSalmon	orange
DarkOrange	coral		LightCoral	tomato		OrangeRed
red		HotPink		DeepPink	pink		LightPink
PaleVioletRed	maroon		MediumVioletRed	VioletRed	magenta
violet		plum		orchid		MediumOrchid	DarkOrchid
DarkViolet	BlueViolet	purple		MediumPurple	thistle
DarkGrey	DarkBlue	DarkCyan	DarkMagenta	DarkRed
LightGreen
@end verbatim


@subsubheading Color names with a numerical suffix

In the following names the suffix N can be a number in the range 1-4:

@verbatim
snowN		seashellN	AntiqueWhiteN	bisqueN		PeachPuffN
NavajoWhiteN	LemonChiffonN	cornsilkN	ivoryN		honeydewN
LavenderBlushN	MistyRoseN	azureN		SlateBlueN	RoyalBlueN
blueN		DodgerBlueN	SteelBlueN	DeepSkyBlueN	SkyBlueN
LightSkyBlueN	LightSteelBlueN	LightBlueN	LightCyanN	PaleTurquoiseN
CadetBlueN	turquoiseN	cyanN		aquamarineN	DarkSeaGreenN
SeaGreenN	PaleGreenN	SpringGreenN	greenN		chartreuseN
OliveDrabN	DarkOliveGreenN	khakiN		LightGoldenrodN	LightYellowN
yellowN		goldN		goldenrodN	DarkGoldenrodN	RosyBrownN
IndianRedN	siennaN		burlywoodN	wheatN		tanN
chocolateN	firebrickN	brownN		salmonN		LightSalmonN
orangeN		DarkOrangeN	coralN		tomatoN		OrangeRedN
redN		DeepPinkN	HotPinkN	pinkN		LightPinkN
PaleVioletRedN	maroonN		VioletRedN	magentaN	orchidN
plumN		MediumOrchidN	DarkOrchidN	purpleN		MediumPurpleN
thistleN
@end verbatim


@subsubheading Grey Scale

A grey scale can be obtained using:

@example
greyN
@end example

@noindent
Where N is in the range 0-100.


@node The Feta font
@appendixsec The Feta font

@cindex Feta font
@cindex Font, Feta

The following symbols are available in the Emmentaler font and may be
accessed directly using text markup such as @code{g^\markup @{
\musicglyph #"scripts.segno" @}}, see @ref{Formatting text}.

@lilypondfile[noindent]{font-table.ly}


@node Note head styles
@appendixsec Note head styles

@cindex note head styles
The following styles may be used for note heads.

@lilypondfile[noindent]{note-head-style.ly}

@include markup-commands.tely

@include markup-list-commands.tely

@node List of articulations
@appendixsec List of articulations

@cindex accent
@cindex marcato
@cindex staccatissimo
@cindex espressivo
@cindex fermata
@cindex stopped
@cindex staccato
@cindex portato
@cindex tenuto
@cindex upbow
@cindex downbow
@cindex foot marks
@cindex organ pedal marks
@cindex turn
@cindex open
@cindex stopped
@cindex flageolet
@cindex reverseturn
@cindex trill
@cindex prall
@cindex mordent
@cindex prallprall
@cindex prallmordent
@cindex prall, up
@cindex prall, down
@cindex thumb marking
@cindex segno
@cindex coda
@cindex varcoda

Here is a chart showing all scripts available,

@lilypondfile[ragged-right,quote]{script-chart.ly}



@node All context properties
@appendixsec All context properties

@include context-properties.tely


@node Layout properties
@appendixsec Layout properties

@include layout-properties.tely


@node Identifiers
@appendixsec Identifiers

@include identifiers.tely


@node Scheme functions
@appendixsec Scheme functions

@include scheme-functions.tely

