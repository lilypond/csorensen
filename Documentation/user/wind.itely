@c -*- coding: utf-8; mode: texinfo; -*-
@ignore
    Translation of GIT committish: FILL-IN-HEAD-COMMITTISH

    When revising a translation, copy the HEAD committish of the
    version that you are working on.  See TRANSLATION for details.
@end ignore

@c \version "2.11.38"

@node Wind instruments
@section Wind instruments

This section includes extra information for writing for bagpipes.

@menu
* Common notation for wind instruments::
* Bagpipes::
@end menu

@node Common notation for wind instruments
@subsection Common notation for wind instruments

@menu
* References for wind instruments::
* Half-holes::
@end menu

@node References for wind instruments
@subsubsection References for wind instruments

@c TODO Add refs
TBC

@node Half-holes
@subsubsection Half-holes
@c TODO Add text; search -user
TBC

@node Bagpipes
@subsection Bagpipes

@menu
* Bagpipe definitions::
* Bagpipe example::
@end menu

@node Bagpipe definitions
@subsubsection Bagpipe definitions

LilyPond contains special definitions for music for the Scottish
highland bagpipe; to use them, add

@example
\include "bagpipe.ly"
@end example

@noindent
at the top of your input file.  This lets you add the special grace notes
common to bagpipe music with short commands.  For example, you could
write @code{\taor} instead of

@example
\grace @{ \small G32[ d G e] @}
@end example

@code{bagpipe.ly} also contains pitch definitions for the bagpipe
notes in the appropriate octaves, so you do not need to worry about
@code{\relative} or @code{\transpose}.

@lilypond[ragged-right,verbatim,quote,notime]
\include "bagpipe.ly"
{ \grg G4 \grg a \grg b \grg c \grg d \grg e \grg f \grA g A }
@end lilypond

Bagpipe music nominally uses the key of D Major (even though that
isn't really true).  However, since that is the only key that can be used,
the key signature is normally not written out.  To set this up correctly,
always start your music with @code{\hideKeySignature}.  If you for some
reason want to show the key signature, you can use @code{\showKeySignature}
instead.

Some modern music use cross fingering on c and f to flatten those notes.
This can be indicated by @code{cflat} or @code{fflat}.  Similarly, the
piobaireachd high g can be written @code{gflat} when it occurs in light
music.


@node Bagpipe example
@subsubsection Bagpipe example

This is what the well known tune Amazing Grace looks like in bagpipe
notation.

@lilypond[verbatim,quote]
\include "bagpipe.ly"
\layout {
  indent = 0.0\cm
  \context { \Score \remove "Bar_number_engraver" }
}

\header {
  title = "Amazing Grace"
  meter = "Hymn"
  arranger = "Trad. arr."
}

{
  \hideKeySignature
  \time 3/4
  \grg \partial 4 a8. d16
  \slurd d2 \grg f8[ e32 d16.]
  \grg f2 \grg f8 e
  \thrwd d2 \grg b4
  \grG a2 \grg a8. d16
  \slurd d2 \grg f8[ e32 d16.]
  \grg f2 \grg e8. f16
  \dblA A2 \grg A4
  \grg A2 f8. A16
  \grg A2 \hdblf f8[ e32 d16.]
  \grg f2 \grg f8 e
  \thrwd d2 \grg b4
  \grG a2 \grg a8. d16
  \slurd d2 \grg f8[ e32 d16.]
  \grg f2 e4
  \thrwd d2.
  \slurd d2
  \bar "|."
}
@end lilypond





