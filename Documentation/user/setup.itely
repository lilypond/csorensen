@c -*- coding: utf-8; mode: texinfo; -*-
@c This file is part of lilypond-program.tely
@ignore
    Translation of GIT committish: FILL-IN-HEAD-COMMITTISH

    When revising a translation, copy the HEAD committish of the
    version that you are working on.  See TRANSLATION for details.
@end ignore

@c \version "2.11.38"

@node Setup
@chapter Setup

This chapter discusses various post-install configuration options for
LilyPond and various other programs.  This chapter may be safely treated
as a reference: only read a section if it applies to you.

@menu
* Setup for specific Operating Systems::  
* Text editor support::         
* Point and click::             
@end menu


@node Setup for specific Operating Systems
@section Setup for specific Operating Systems

This section explains how to perform additional setup for specific
operating systems.

@menu
* MacOS X on the command-line::  
@end menu


@node MacOS X on the command-line
@subsection MacOS X on the command-line

The scripts (such as lilypond-book, convert-ly, abc2ly, and even
lilypond itself) are included inside the .app file for MacOS@tie{}X.  They can be run from
the command line by invoking them directly, e.g.

@example
@var{path/to}/LilyPond.app/Contents/Resources/bin/lilypond
@end example

@noindent
The same is true of the other scripts in that directory, including
lilypond-book, convert-ly, abc2ly, etc.

Alternatively, you may create scripts which add the path
automatically.  Create a directory to store these scripts,

@example
mkdir -p ~/bin
cd ~/bin
@end example

Create a file called @code{lilypond} which contains

@example
exec @var{path/to}/LilyPond.app/Contents/Resources/bin/lilypond "$@@"
@end example

Create similar files @code{lilypond-book}, @code{convert-ly}, and
any other helper programs you use (@code{abc2ly}, @code{midi2ly},
etc).  Simply replace the @code{bin/lilypond} with
@code{bin/convert-ly} (or other program name) in the above file.

Make the file executable,

@example
chmod u+x lilypond
@end example

Now, add this directory to your path.  Modify (or create)
a file called @code{.profile} in your home directory such that it contains

@example
export PATH=$PATH:~/bin
@end example

@noindent
This file should end with a blank line.

Note that @var{path/to} will generally be @code{/Applications/}.


@node Text editor support
@section Text editor support

@cindex editors
@cindex vim
@cindex emacs
@cindex modes, editor
@cindex syntax coloring
@cindex coloring, syntax

There is support from different text editors for LilyPond.

@menu
* Emacs mode::                  
* Vim mode::                    
* jEdit::                       
* TexShop::                     
* TextMate::                    
@end menu

@node Emacs mode
@subsection Emacs mode

Emacs has a @file{lilypond-mode}, which provides keyword
autocompletion, indentation, LilyPond specific parenthesis matching
and syntax coloring, handy compile short-cuts and reading LilyPond
manuals using Info.  If @file{lilypond-mode} is not installed on your
platform, see below.

An Emacs mode for entering music and running LilyPond is contained in
the source archive in the @file{elisp} directory.  Do @command{make
install} to install it to @var{elispdir}.  The file @file{lilypond-init.el}
should be placed to @var{load-path}@file{/site-start.d/} or appended
to your @file{~/.emacs} or @file{~/.emacs.el}.

As a user, you may want add your source path (e.g. @file{~/site-lisp/}) to
your @var{load-path} by appending the following line (as modified) to your
@file{~/.emacs}

@c any reason we do not advise:  (push "~/site-lisp" load-path)
@example
(setq load-path (append (list (expand-file-name "~/site-lisp")) load-path))
@end example


@node Vim mode
@subsection Vim mode

For @uref{http://@/www@/.vim@/.org,VIM}, a @file{vimrc} is supplied,
along with syntax coloring tools.  A Vim mode for entering music and
running LilyPond is contained in the source archive in @code{$VIM}
directory.

The LilyPond file type is detected if the file
@file{~/.vim/filetype.vim} has the following content

@example
if exists("did_load_filetypes")
  finish
endif
augroup filetypedetect
  au! BufNewFile,BufRead *.ly           setf lilypond
augroup END
@end example

Please include this path by appending the following line to your
@file{~/.vimrc}

@example
set runtimepath+=/usr/local/share/lilypond/$@{LILYPOND_VERSION@}/vim/
@end example

@noindent
where $@{LILYPOND_VERSION@} is your LilyPond version.  If LilyPond was not
installed in @file{/usr/local/}, then change this path accordingly.


@node jEdit
@subsection jEdit

Created as a plugin for the @uref{http://@/www@/.jedit@/.org@/,jEdit}
text editor, LilyPondTool is the most feature-rich text-based tool for
editing LilyPond scores.  Its features include a Document Wizard with
lyrics support to set up documents easier, and embedded PDF viewer with
advanced point-and-click support.  For screenshots, demos and
installation instructions, visit
@uref{http://lilypondtool@/.organum@/.hu}


@node TexShop
@subsection TexShop

The @uref{http://@/www@/.uoregon@/.edu/~koch/texshop/index@/.html,TexShop}
editor for MacOS@tie{}X can be extended to run LilyPond, lilypond-book and
convert-ly from within the editor, using the extensions available at 
@uref{http://@/www@/.dimi@/.uniud@/.it/vitacolo/freesoftware@/.html}.


@node TextMate
@subsection TextMate

There is a LilyPond bundle for TextMate.  It may be installed by running

@example
mkdir -p /Library/Application\ Support/TextMate/Bundles
cd /Library/Application\ Support/TextMate/Bundles
svn co http://macromates.com/svn/Bundles/trunk/Bundles/Lilypond.tmbundle/
@end example



@node Point and click
@section Point and click
@cindex point and click


Point and click lets you find notes in the input by clicking on them
in the PDF viewer.  This makes it easier to find input that causes
some error in the sheet music.

When this functionality is active, LilyPond adds hyperlinks to the PDF
file.  These hyperlinks are sent to the web-browser, which opens a
text-editor with the cursor in the right place.

To make this chain work, you should configure your PDF viewer to
follow hyperlinks using the @file{lilypond-invoke-editor} script
supplied with LilyPond.

For Xpdf on UNIX, the following should be present in
@file{xpdfrc}@footnote{On UNIX, this file is found either in
@file{/etc/xpdfrc} or as @file{.xpdfrc} in your home directory.}

@example
urlCommand     "lilypond-invoke-editor %s"
@end example

The program @file{lilypond-invoke-editor} is a small helper
program.  It will invoke an editor for the special @code{textedit}
URIs, and run a web browser for others.  It tests the environment
variable @code{EDITOR} for the following patterns,

@table @code
@item emacs
  this will invoke
@example
emacsclient --no-wait +@var{line}:@var{column} @var{file}
@end example
@item vim
  this will invoke
@example
gvim --remote +:@var{line}:norm@var{char} @var{file}
@end example

@item nedit
this will invoke
@example
  nc -noask +@var{line} @var{file}'
@end example
@end table

The environment variable @code{LYEDITOR} is used to override this.  It
contains the command line to start the editor, where @code{%(file)s},
@code{%(column)s}, @code{%(line)s} is replaced with the file, column
and line respectively.  The  setting

@example
emacsclient --no-wait +%(line)s:%(column)s %(file)s
@end example

@noindent
for @code{LYEDITOR} is equivalent to the standard emacsclient
invocation.


@cindex file size, output

The point and click links enlarge the output files significantly.  For
reducing the size of PDF and PS files, point and click may be switched
off by issuing

@example
#(ly:set-option 'point-and-click #f)
@end example

@noindent
in a @file{.ly} file.  Alternately, you may pass this as an command-line
option

@example
lilypond -dno-point-and-click file.ly
@end example

