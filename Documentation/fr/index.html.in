<html>
<!--
    Translation of GIT committish: d8f1a358802cf74903eb2dfa7ce761ca4803c634

    When revising a translation, copy the HEAD committish of the
    version that you are working on.  See TRANSLATION for details.
!-->
  <head>
    <title>Documentation de LilyPond @TOPLEVEL_VERSION@</title>
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
    <meta name="aesop" content="links">
    <meta name="description"
          content="Top-level index to the standard documentation for
                   LilyPond @TOPLEVEL_VERSION@">
    <style type="text/css">
    .navigation         { background-color: #e8ffe8;
   	                  padding: 2; border: #c0ffc0 1px solid;
			  text-align: center;
                          font-weight: bold;
                          font-family: avantgarde, sans-serif;
                          font-size: 110%; }
      a.title { font-weight: bold; font-size: 110%; }
      ul { margin-left: 1em; padding: 0pt; border: 0pt; }
      ul li { margin-top: 0.2em; }
      td.left-column { padding-right: 1em; }
      td.right-column { padding-left: 1em; }
      hr { border:0; height:1; color: #000000; background-color: #000000; }
    </style> 
  </head>
  
  <body>
        
	<div class="navigation">
	  <h1>Documentation de LilyPond</h1>
	</div>
    <div align="center" class="titlepage">

      <p>
	<strong>Version @TOPLEVEL_VERSION@</strong>
	<br>
	<strong>@DATE@</strong>
      </p>
    </div>

    <table align="center">
      <tbody>
        <tr>
          <td class="left-column">
	  <ul>
	    <li>
	    <a class="title"
	      href="user/lilypond-learning/index.fr.html">Manuel d'initiation</a>
	      <br>&mdash; commencer ici
	    <li><a class="title" href="user/music-glossary/index.html">Glossaire</a>
	      multilingue<br>(aussi en
	      <a class="title" href="user/music-glossary-big-page.html">page unique</a>
	      ~ 1 Mb ou <a class="title" href="user/music-glossary.pdf">au format PDF</a>)
	      <br>&mdash; vocabulaire théorique de la musique
	  </ul>
	  </td>
	  <td class="right-column">
	  <ul>
            <li><a class="title" href="topdocs/NEWS.html">Nouveautés</a>
 	      <br>&mdash; changements depuis la version majeure précédente
	    <li><a class="title" href="../examples.html">Exemples</a>
	      <br>&mdash; quelques exemples
	  </ul>
	  </td>
        </tr>
        <tr>
          <td valign="baseline" class="left-column">
          &nbsp;
	  <ul>
	    <li>
	      <a class="title" href="user/lilypond/index.fr.html">Manuel de notation</a>
	      (partiellement traduit, aussi<br>en <a class="title"
				href="user/lilypond-big-page.html">page unique
 	        en anglais</a> ~ 4 Mb, ou
	        <a class="title" href="user/lilypond.fr.pdf">au format PDF</a>)
		<br>&mdash; gravure de musique avec LilyPond
	    <li>
	      <a  class="title" href="user/lilypond-internals/index.html">Référence des propriétés internes</a>
	      (en anglais, en <a class="title" href="user/lilypond-internals-big-page.html">page
		unique</a> ~ 1 Mb)
	      <br>&mdash; définitions pour les retouches
	  </ul>
	  </td>
	  <td valign="baseline" class="right-column">
          &nbsp;
	  <ul>
            <li>
	    <a class="title" href="user/lilypond-program/index.fr.html">Utilisation des programmes</a>
(partiellement traduit, aussi<br>en <a class="title"
		href="user/lilypond-program-big-page.html">page unique en anglais</a>, ou
	    <a class="title" href="user/lilypond-program.fr.pdf">au format PDF</a>)
 	      <br>&mdash; installation et exécution des programmes
	    <li><a class="title" href="../input/lsr/lilypond-snippets/index.html">Exemples de code</a>
(en <a class="title" href="../input/lsr/lilypond-snippets-big-page.html">une seule grande page</a>,
au format <a class="title" href="user/lilypond-snippets.pdf">PDF</a>)
	      <br>&mdash; petits trucs, astuces et exemples
	  </ul>
	  </td>
        </tr>
        <tr>
          <td valign="baseline" class="left-column">
          &nbsp;
       <ul>
            <li><a class="title"  href="devel.html">Ressources de développement</a>
              <br>&mdash; documentation pour les développeurs et contributeurs
       </ul>
    </td><td class="right-column">
          &nbsp;
<!-- TODO
    <ul>
        <li><a class="title"  href="translations.html">État des traductions</a>
        <br>&mdash; avancement des traductions
    </ul>
-->
         </td>
        </tr>
	<tr>
          <td valign="baseline" class="left-column">
	  &nbsp;
          <ul>
            <li> <a class="title"  href="http://lilypond.org/">lilypond.org</a>
              <br>&mdash; le site Web
            <li>
	      <a class="title" href="http://www.gnu.org/copyleft/gpl.html">Licence</a>
	      <br>&mdash; la licence GNU GPL
          </ul>
          </td>
          <td valign="baseline" class="right-column">
          &nbsp;
          <ul>
	    <li><a class="title"  href="THANKS.html">Remerciements</a>
	      <br>&mdash; à nos contributeurs
	    <li><a class="title"  href="DEDICATION.html">Dédicace</a>
	      <br>&mdash; par Jan et Han-Wen
          </ul>
          </td>
        </tr>
         <tr>
          <td valign="baseline" class="left-column">
          &nbsp;
          <ul>
          </ul>
          </td>
        </tr>
      </tbody>
    </table>
    <p><strong>NOTE</strong>&nbsp;: vous pouvez trouver, au bas de
    chaque page de cette documentation, des liens vers les traductions
    disponibles.
    </p>
  </body>
</html>
