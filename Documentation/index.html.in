<html>
<!--
    Translation of GIT committish: FILL-IN-HEAD-COMMITTISH

    When revising a translation, copy the HEAD committish of the
    version that you are working on.  See TRANSLATION for details.
!-->
  <head>
    <title>LilyPond @TOPLEVEL_VERSION@ documentation</title>
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
    <meta name="aesop" content="links">
    <meta name="description"
          content="Top-level index to the standard documentation for
                   LilyPond @TOPLEVEL_VERSION@">
    <style type="text/css">
    .navigation         { background-color: #e8ffe8;
   	                  padding: 2; border: #c0ffc0 1px solid;
			  text-align: center;
                          font-weight: bold;
                          font-family: avantgarde, sans-serif;
                          font-size: 110%; }
      a.title { font-weight: bold; font-size: 110%; }
      ul { margin-left: 1em; padding: 0pt; border: 0pt; }
      ul li { margin-top: 0.2em; }
      td.left-column { padding-right: 1em; }
      td.right-column { padding-left: 1em; }
      hr { border:0; height:1; color: #000000; background-color: #000000; }
    </style> 
  </head>
  
  <body>
        
	<div class="navigation">
	  <h1>LilyPond documentation</h1>
	</div>
    <div align="center" class="titlepage">

      <p>
	<strong>Version @TOPLEVEL_VERSION@</strong>
	<br>
	<strong>@DATE@</strong>
      </p>
    </div>

    <table align="center">
      <tbody>
        <tr>
          <td class="left-column">
	  <ul>
	    <li>
	    <a class="title"
href="user/lilypond-learning/index.html">Learning Manual (LM)</a>
	      <br>(start here)

	   <li><a class="title"
href="user/music-glossary/index.html">Music Glossary (MG)</a>
(in <a class="title" href="user/music-glossary-big-page.html">one big page</a> ~ 1 Mb,
in <a class="title" href="user/music-glossary.pdf">PDF</a>)

     <br>(for non-English users)
	</ul>
	  </td>
	  <td class="right-column">
	  <ul>
	          <li>
	    <a class="title" href="topdocs/NEWS.html">News</a>
 	    <br>(changes since the previous major release)

<li><a class="title" href="../examples.html">Examples</a>
     <br>(see some examples)

	  </ul>
	  </td>
        </tr>
        <tr>
          <td valign="baseline" class="left-column">
          &nbsp;
	  <ul>
	    <li>
<a class="title" href="user/lilypond/index.html">Notation
Reference (NR)</a>
(in <a class="title" href="user/lilypond-big-page.html">one big page</a> ~ 4 Mb,
in <a class="title" href="user/lilypond.pdf">PDF</a>)
     <br>(writing music in LilyPond)

       <li>
 <a  class="title"
href="user/lilypond-internals/index.html">Internals Reference (IR)</a>
 (in <a class="title" href="user/lilypond-internals-big-page.html">one big page</a> ~ 1 Mb)
     <br>(definitions for tweaking)
	 
  </ul>
	  </td>
	  <td valign="baseline" class="right-column">
          &nbsp;
	  <ul>
              <li>
	    <a class="title"
href="user/lilypond-program/index.html">Application Usage (AU)</a>
(in <a class="title" href="user/lilypond-program-big-page.html">one big page</a>,
in <a class="title" href="user/lilypond-program.pdf">PDF</a>)
	    <br>(how to install and run the program)

<li><a class="title"href="../input/lsr/lilypond-snippets/index.html">Snippets List (SL)</a>
(in <a class="title" href="../input/lsr/lilypond-snippets-big-page.html">one big page</a>,
in <a class="title" href="user/lilypond-snippets.pdf">PDF</a>)
     <br>(Short tricks, tips, and examples)

	</ul>

    </td>
        <tr>
          <td valign="baseline" class="left-column">
          &nbsp;
	<ul>
            <li><a class="title"  href="devel.html">Developers resources</a>
              <br>(documentation for developers and contributors)
	</ul>
    </td><td class="right-column">
          &nbsp;
<!-- TODO
    <ul>
        <li><a class="title"  href="translations.html">Translations status</a>
        <br>(see translations progress)
    </ul>
-->
	  </td>
        </tr>
        <tr>
          <td valign="baseline" class="left-column">
          &nbsp;
	<ul>

            <li> <a class="title"  href="http://lilypond.org/">lilypond.org</a>
              <br>(the website)
            <li>
	      <a class="title" href="http://www.gnu.org/copyleft/gpl.html">License</a>
	      <br> (the GNU GPL) 

	</ul>
    </td><td class="right-column">
          &nbsp;
    <ul>
        <li><a class="title"  href="THANKS.html">Thankyous</a>
        <br>(to our contributors)

        <li><a class="title"  href="DEDICATION.html">Dedication</a>
        <br>(by Jan and Han-Wen)

	  </ul>
	  </td>
        </tr>
      </tbody>
    </table>
    <p><strong>NOTE</strong>: like every HTML page in this
      documentation, you can find at bottom links to translations of
      this page in other languages.
    </p>
  </body>
</html>
