\version "2.10.0"
% Symbols needed to print accordion music
% 
%  2' = T
%  4' = F
%  8' = E
% 16' = S
%

accDiscant = #(make-articulation "accDiscant")
accDiscantF = #(make-articulation "accDiscantF")
accDiscantE = #(make-articulation "accDiscantE")
accDiscantEh = #(make-articulation "accDiscantEh")
accDiscantFE = #(make-articulation "accDiscantFE")
accDiscantFEh = #(make-articulation "accDiscantFEh")
accDiscantEE = #(make-articulation "accDiscantEE")
accDiscantFEE = #(make-articulation "accDiscantFEE")
accDiscantEEE = #(make-articulation "accDiscantEEE")
accDiscantFEEE = #(make-articulation "accDiscantFEEE")
accDiscantS = #(make-articulation "accDiscantS")
accDiscantFS = #(make-articulation "accDiscantFS")
accDiscantES = #(make-articulation "accDiscantES")
accDiscantEhS = #(make-articulation "accDiscantEhS")
accDiscantFES = #(make-articulation "accDiscantFES")
accDiscantFEhS = #(make-articulation "accDiscantFEhS")
accDiscantEES = #(make-articulation "accDiscantEES")
accDiscantFEES = #(make-articulation "accDiscantFEES")
accDiscantEEES = #(make-articulation "accDiscantEEES")
accDiscantFEEES = #(make-articulation "accDiscantFEEES")
accDiscantSS = #(make-articulation "accDiscantSS")
accDiscantESS = #(make-articulation "accDiscantESS")
accDiscantEESS = #(make-articulation "accDiscantEESS")
accDiscantEEESS = #(make-articulation "accDiscantEEESS")

accFreebase = #(make-articulation "accFreebase")
accFreebaseF = #(make-articulation "accFreebaseF")
accFreebaseE = #(make-articulation "accFreebaseE")
accFreebaseFE = #(make-articulation "accFreebaseFE")

accBayanbase = #(make-articulation "accBayanbase")
accBayanbaseT = #(make-articulation "accBayanbaseT")
accBayanbaseE = #(make-articulation "accBayanbaseE")
accBayanbaseTE = #(make-articulation "accBayanbaseTE")
accBayanbaseEE = #(make-articulation "accBayanbaseEE")
accBayanbaseTEE = #(make-articulation "accBayanbaseTEE")

accStdbase = #(make-articulation "accStdbase")
accStdbaseFE = #(make-articulation "accStdbaseFE")
accStdbaseTFE = #(make-articulation "accStdbaseTFE")
accStdbaseMES = #(make-articulation "accStdbaseMES")
accStdbaseTFMES = #(make-articulation "accStdbaseTFMES")

accSB = #(make-articulation "accSB")
accBB = #(make-articulation "accBB")
accOldEE = #(make-articulation "accOldEE")
accOldEES = #(make-articulation "accOldEES")
