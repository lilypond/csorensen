\header {
  texidoc = "chord tremolos don't collide with whole
notes."
}

\version "2.10.19"

\relative c'''{
  \repeat tremolo 32{ g64 a }
} 
