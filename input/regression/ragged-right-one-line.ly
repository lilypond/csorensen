\version "2.11.44"

\header {
  texidoc = "When a score takes up only a single line and it is stretched, it
is printed as ragged by default."
}

{ a b c d }