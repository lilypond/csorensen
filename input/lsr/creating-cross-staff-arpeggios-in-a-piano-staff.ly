%% Do not edit this file; it is auto-generated from LSR http://lsr.dsi.unimi.it
%% This file is in the public domain.
\version "2.11.48"

\header {
  lsrtags = "expressive-marks"

  texidoc = "
In a @code{PianoStaff}, it is possible to let an arpeggio cross between
the staves by setting the property @code{PianoStaff.connectArpeggios}.
 

"
  doctitle = "Creating cross-staff arpeggios in a piano staff"
} % begin verbatim
\new PianoStaff \relative c'' <<
  \set PianoStaff.connectArpeggios = ##t
  \new Staff {
    <c e g c>4\arpeggio
    <g c e g>4\arpeggio
    <e g c e>4\arpeggio
    <c e g c>4\arpeggio
  }
  \new Staff {
    \clef bass
    \repeat unfold 4 {
      <c,, e g c>4\arpeggio
    }
  }
>>
