%% Do not edit this file; it is auto-generated from LSR http://lsr.dsi.unimi.it
%% This file is in the public domain.
\version "2.11.48"

\header {
  lsrtags = "simultaneous-notes, tweaks-and-overrides"

  texidoc = "
When the typesetting engine cannot cope, the @code{force-hshift}
property of the @code{NoteColumn} object can be used to override
typesetting decisions. The measure units used here are staff spaces.

"
  doctitle = "Forcing horizontal shift of notes"
} % begin verbatim
\relative c' <<
  {
    <d g>2 <d g>
  }
  \\
  { <b f'>2
    \once \override NoteColumn #'force-hshift = #1.7
    <b f'>2
  }
>>
