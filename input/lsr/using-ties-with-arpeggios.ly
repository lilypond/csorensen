%% Do not edit this file; it is auto-generated from LSR http://lsr.dsi.unimi.it
%% This file is in the public domain.
\version "2.11.48"

\header {
  lsrtags = "rhythms"

  texidoc = "
Ties are sometimes used to write out arpeggios.  In this case, two tied
notes need not be consecutive.  This can be achieved by setting the
@code{tieWaitForNote} property to \"true\".  The same feature is also
useful, for example, to tie a tremolo to a chord, but in principle, it
can also be used for ordinary consecutive notes, as demonstrated in
this example. 

"
  doctitle = "Using ties with arpeggios"
} % begin verbatim
\relative c' {
  \set tieWaitForNote = ##t
  \grace { c16[~ e~ g]~ } <c, e g>2
  \repeat tremolo 8 { c32~ c'~ } <c c,>1
  e8~ c~ a~ f~ <e' c a f>2
  \tieUp c8~ a \tieDown \tieDotted g~ c g2
}
