%% Do not edit this file; it is auto-generated from LSR http://lsr.dsi.unimi.it
%% This file is in the public domain.
\version "2.11.48"

\header {
  lsrtags = "template"

  texidoc = "
This very simple template gives you a staff with notes, suitable for a
solo instrument or a melodic fragment. Cut and paste this into a file,
add notes, and you're finished! 

"
  doctitle = "Single staff template with only notes"
} % begin verbatim
melody = \relative c' {
  \clef treble
  \key c \major
  \time 4/4
  
  a4 b c d
}

\score {
  \new Staff \melody
  \layout { }
  \midi { }
}
